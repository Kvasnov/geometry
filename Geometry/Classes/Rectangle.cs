﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geometry.Interfaces;

namespace Geometry.Classes
{
    public class Rectangle : IGeometricFigure
    {
        private readonly double _height;
        private readonly double _width;

        public Rectangle (double height, double width)
        {
            if (height <= 0 || height <= 0)
            {
                throw new ArgumentOutOfRangeException("Side of rectangle should not be negative.");
            }

            _height = height;
            _width = width;
        }

        public double Perimeter()
        {
            return 2 * (_height + _width);
        }

        public double FigureArea()
        {
            return _height * _width;
        }
    }
}
