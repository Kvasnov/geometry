﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geometry.Interfaces;

namespace Geometry.Classes
{
    public class Circle : IGeometricFigure
    {
        private readonly double _radius;

        public Circle(double radius)
        {
            if (radius <= 0)
            {
                throw new ArgumentOutOfRangeException("Radius should not be negative.");
            }

            _radius = radius;
        }
        
        public double Perimeter()
        {
            return 2 * _radius * Math.PI;
        }

        public double FigureArea()
        {
            return Math.PI * _radius * _radius;
        }
    }
}
