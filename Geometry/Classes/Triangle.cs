﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geometry.Interfaces;

namespace Geometry.Classes
{
    public class Triangle : IGeometricFigure
    {
        private readonly double _cathetus1;
        private readonly double _cathetus2;
        private readonly double _hypotenuse;

        public Triangle(double cathetus1, double cathetus2, double hypotenuse)
        {
            if (cathetus1 <= 0 || cathetus2 <= 0 || hypotenuse <= 0)
            {
                throw new ArgumentOutOfRangeException("Side of triangle should not be negative.");
            }

            _cathetus1 = cathetus1;
            _cathetus2 = cathetus2;
            _hypotenuse = hypotenuse;
        }

        public double Perimeter()
        {
            return _cathetus1 + _cathetus2 + _hypotenuse;
        }

        public double FigureArea()
        {
            return 1.0/2.0 * _cathetus1 * _cathetus2;
        }
    }
}
