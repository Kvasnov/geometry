﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometry.Classes
{
    abstract class Figure
    {
        public abstract double Perimeter();

        public abstract double FigureArea();
    }
}
