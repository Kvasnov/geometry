﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometry.Interfaces
{
    public interface IGeometricFigure
    {
        double Perimeter();

        double FigureArea();
    }
}
