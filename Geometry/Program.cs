﻿/*
Задание:
Нужно консольное приложение. 
Реализовать несколько классов геометрических фигур (например, прямоугольник, треугольник, круг). 
Реализовать для них параметризованные конструкторы (с нужными аргументами для определения их размеров). 
Реализовать методы получениях их периметров и площадей. Создать коллекцию, хранящую фигуры различных классов. 
Реализовать метод получения суммарной площади всех фигур в коллекции. 
Получить фигуры с наименьшей площадью и с наибольшей площадью.

LINQ
var allPerimeters = figures.Sum(t => t.Perimeter());
var maxArea = figures.Max(t => t.FigureArea());
var minArea = figures.Min(t => t.FigureArea());


 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geometry.Classes;
using Geometry.Interfaces;

namespace Geometry
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle1 = new Rectangle(1, 1);
            Rectangle rectangle2 = new Rectangle(2, 2);
            Rectangle rectangle3 = new Rectangle(3, 3);

            Triangle triangle1 = new Triangle(1, 2, 2.2361);
            Triangle triangle2 = new Triangle(2, 2, 2.8284);
            Triangle triangle3 = new Triangle(3, 4, 5);
            
            Circle circle1 = new Circle(1);
            Circle circle2 = new Circle(2);
            Circle circle3 = new Circle(3);

            var figures = new List<IGeometricFigure>();

            figures.Add(rectangle1);
            figures.Add(rectangle2);
            figures.Add(rectangle3);
            figures.Add(triangle1);
            figures.Add(triangle2);
            figures.Add(triangle3);
            figures.Add(circle1);
            figures.Add(circle2);
            figures.Add(circle3);

            //double allPerimeters = 0;
            //double maxArea = Double.MinValue;
            //double minArea = Double.MaxValue;

            //foreach (var figure in figures)
            //{
            //    allPerimeters += figure.Perimeter();

            //    if (maxArea < figure.FigureArea())
            //    {
            //        maxArea = figure.FigureArea();
            //    }
            //    else if (minArea > figure.FigureArea())
            //    {
            //        minArea = figure.FigureArea();
            //    }
            //}

            var allPerimeters = figures.Sum(t => t.Perimeter());
            var maxArea = figures.Max(t => t.FigureArea());
            var minArea = figures.Min(t => t.FigureArea());


            Console.WriteLine($" Общий периметр: {allPerimeters}");
            Console.WriteLine($" Минимальная площадь: {minArea}");
            Console.WriteLine($" Максимальная площадь: {maxArea}");

            Console.ReadKey();
        }
    }
}
